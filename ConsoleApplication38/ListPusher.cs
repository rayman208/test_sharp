﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication38
{
    class ListPusher
    {
        private List<int> ints;

        public ListPusher()
        {
            ints = new List<int>();
        }

        public void AddToList(int i)
        {
            ints.Add(i);
        }

        public void ClearList()
        {
            ints.Clear();
        }

        private void EvenExists()
        {
            while (true)
            {
                bool exist = false;
                foreach (var i in ints)
                {
                    if (i % 2 == 0)
                    {
                        exist = true;
                        break;
                    }
                }
                Console.WriteLine(exist);
            }
        }

        public void CheckEven()
        {
            Task.Factory.StartNew(() => EvenExists());
        }
    }
}
