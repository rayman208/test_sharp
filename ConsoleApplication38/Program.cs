﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApplication38
{
    class Program
    {
        static int summaryCount = 0;

        static void DowloadPage(string url)
        {
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            int currentCount = webClient.DownloadString(url).Length;

            summaryCount += currentCount;
            //Console.WriteLine(Thread.CurrentThread.ManagedThreadId+" "+url + " " + currentCount + " " + summaryCount);
        }

        static void DowloadPages(List<string> urls)
        {
            /*Task[] tasks = new Task[urls.Count];
            int i = 0; 
            foreach (string url in urls)
            {
                tasks[i] = Task.Factory.StartNew(() => DowloadPage(url));
                i++;
            }
            Task.WaitAll(tasks);*/
            
            Parallel.ForEach(urls, DowloadPage);

            /*foreach (string url in urls)
             {
                 DowloadPage(url);
             }*/
        }

        static void Main(string[] args)
        {
            //skldfbkjsdfksdjf
            List<string> urls = new List<string>()
            {
                "https://bash.im/best",
                "https://ithappens.me/best/2015/12",
                "https://zadolba.li/best",
                "https://wiki.postgresql.org/wiki/Community_Generated_Articles,_Guides,_and_Documentation",
                "https://en.wikipedia.org/wiki/PostgreSQL"
            };

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Reset();

            stopwatch.Start();

            DowloadPages(urls);

            stopwatch.Stop();

            Console.WriteLine(stopwatch.Elapsed);
            Console.WriteLine(summaryCount);

            Console.WriteLine("hello");

            Console.ReadKey();
        }
    }
}
